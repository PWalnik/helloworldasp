﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace helloWorld
{
    public class DbInitializer
    {
        public static void Initialize(NamesContext context)
        {
            context.Database.EnsureCreated();
            if (context.Names.Any())
            {
                return;
            }

            var exampleNames = new Person[]
            {
                new Person{FirstName = "Jan", LastName = "Kowalski", PhoneNumber = "+48564867943"},
                new Person{FirstName = "Mateusz", LastName = "Nowak", PhoneNumber = "+52546376453"}
            };
            foreach(Person m in exampleNames)
            {
                context.Names.Add(m);
            }
            context.SaveChanges();
        }
    }
}
