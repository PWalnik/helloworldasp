﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace helloWorld
{
    public class Person
    {
        [Required(ErrorMessage = "Your must provide a FirstName")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Your must provide a LastName")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Your must provide a PhoneNumber")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:\d{9}|00\d{10}|\+\d{2}\d{9})$", ErrorMessage = "Invalid phone")]
        public string PhoneNumber { get; set; }
        public int ID { get; set; }
    }

    public class NamesContext : DbContext
    {
        public NamesContext(DbContextOptions<NamesContext> options) : base(options)
        {
        }
        public DbSet<Person> Names { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");
        }

    }
}
