using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using System.Text;

namespace helloWorld.Controllers
{
    public class PeopleController : Controller
    {
        private readonly NamesContext _context;

        public PeopleController(NamesContext context)
        {
            _context = context;
        }
        // GET: People
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewData["CurrentSort"] = sortOrder;

            if (String.IsNullOrEmpty(sortOrder))
            {
                ViewData["FirstNameSortParm"] = "fname_desc";
            }
            else if (sortOrder == "fname_desc")
            {
                ViewData["FirstNameSortParm"] = "fname_asc";
            }
            else if (sortOrder == "fname_asc")
            {
                ViewData["FirstNameSortParm"] = "fname_desc";
            }
            if (String.IsNullOrEmpty(sortOrder))
            {
                ViewData["LastNameSortParm"] = "lname_desc";
            }
            else if (sortOrder == "fname_desc")
            {
                ViewData["LastNameSortParm"] = "lname_asc";
            }
            else if (sortOrder == "fname_asc")
            {
                ViewData["LastNameSortParm"] = "lname_desc";
            }
            if (String.IsNullOrEmpty(sortOrder))
            {
                ViewData["PhoneSortParm"] = "phone_desc";
            }
            else if (sortOrder == "phone_desc")
            {
                ViewData["PhoneSortParm"] = "phone_asc";
            }
            else if (sortOrder == "phone_asc")
            {
                ViewData["PhoneSortParm"] = "phone_desc";
            }

            // ViewData["LastNameSortParm"] = sortOrder == ? "lname_desc" : "";
            if (HttpContext.Session.GetString("Login") == null)
                return Redirect("/Home/Index");
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;
            var persons = from p in _context.Names select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                persons = persons.Where(p => p.LastName.Contains(searchString) || p.FirstName.Contains(searchString));
                TempData["Search"] = searchString;
            }
            else
                TempData["Search"] = "null";
            switch (sortOrder)
            {
                case "fname_desc":
                    persons = persons.OrderByDescending(s => s.FirstName);
                    break;
                case "fname_asc":
                    persons = persons.OrderBy(s => s.FirstName);
                    break;
                case "lname_desc":
                    persons = persons.OrderByDescending(s => s.LastName);
                    break;
                case "lname_asc":
                    persons = persons.OrderBy(s => s.LastName);
                    break;
                case "phone_desc":
                    persons = persons.OrderByDescending(s => s.PhoneNumber);
                    break;
                case "phone_asc":
                    persons = persons.OrderBy(s => s.PhoneNumber);
                    break;
                default:                   
                    persons = persons.OrderBy(s => s.FirstName);
                    break;                  
            }

            
            int pageSize = 3;
            int numberOfUsers = _context.Names.Count();
            return View(await PaginatedList<Person>.CreateAsync(persons.AsNoTracking(), page ?? 1,pageSize, numberOfUsers));           
        }
        // GET: People/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (HttpContext.Session.GetString("Login") == null)
                return Redirect("/Home/Index");
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Names
                .SingleOrDefaultAsync(m => m.ID == id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }
        // GET: People/Create
        public IActionResult Create()
        {
            if (HttpContext.Session.GetString("Login") == null)
                return Redirect("/Home/Index");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FirstName,LastName, PhoneNumber")] Person model)
        {
            if (ModelState.IsValid)
            {
                var persons = from p in _context.Names select p;
                persons = persons.Where(p => p.PhoneNumber.Contains(model.PhoneNumber));
                var person = persons.ToArray();
                if (person.Length == 0)
                {
                    model.FirstName = model.FirstName.Trim();
                    model.LastName = model.LastName.Trim();
                    await _context.AddAsync(model);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewData["Warning"] = "This phone number already exists in DataBase, choose another";   
                }
            }
            return View(model);
        }
        // GET: People/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (HttpContext.Session.GetString("Login") == null)
                return Redirect("/Home/Index");
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Names.SingleOrDefaultAsync(m => m.ID == id);
            if (person == null)
            {
                return NotFound();
            }
            return View(person);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FirstName,LastName,PhoneNumber,ID")] Person person)
        {
            if (id != person.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(person);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonExists(person.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(person);
        }
        // GET: People/Delete/5
        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (HttpContext.Session.GetString("Login") == null)
                return Redirect("/Home/Index");
            if (id == null)
            {
                return NotFound();
            }

            var person = await _context.Names
                .SingleOrDefaultAsync(m => m.ID == id);
            if (person == null)
            {
                return NotFound();
            }
            if(saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] = "Delete failed. Try again, and if the problem persists see yor system administrator.";
            }

            return View(person);
        }
        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var person = await _context.Names.AsNoTracking().SingleOrDefaultAsync(m => m.ID == id);
            if(person == null)
            {
                return RedirectToAction("Index");
            }
            try
            {
                _context.Names.Remove(person);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            catch(DbUpdateException)
            {
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
        }
        public FileStreamResult ExportSearchResults()
        {
            string searchText = TempData["Search"].ToString();           
            var persons = from p in _context.Names select p;
            if (searchText != "null")
            {
                persons = persons.Where(p => p.LastName.Contains(searchText) || p.FirstName.Contains(searchText));
            }
            var result = WriteCsvToMemory(persons.AsNoTracking().ToList());
            var memoryStream = new MemoryStream(result);
            TempData["Search"] = searchText;
            return new FileStreamResult(memoryStream, "text/csv") { FileDownloadName = "export.csv" };
        }
        private byte[] WriteCsvToMemory(IEnumerable<Person> records)
        {
            using (var memoryStream = new MemoryStream())
            using (var streamWriter = new StreamWriter(memoryStream, Encoding.UTF8))
            using (var csvWriter = new CsvWriter(streamWriter))
            {
                csvWriter.WriteRecords(records);
                streamWriter.Flush();
                return memoryStream.ToArray();
            }
        }
        private bool PersonExists(int id)
        {
            return _context.Names.Any(e => e.ID == id);
        }
    }
}
