﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System.Linq;

namespace helloWorld.Controllers
{
    public class HomeController : Controller
    {
        private readonly NamesContext _context;
        
        public HomeController(NamesContext context)
        {
            _context = context;
        }     
        public IActionResult Index()
        {
            return View(new Person());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index([Bind("FirstName,LastName, PhoneNumber")]Person model)
        {
            if (ModelState.IsValid)
            {
                var persons = from p in _context.Names select p;
                persons = persons.Where(p => p.PhoneNumber.Contains(model.PhoneNumber));
                var person = persons.ToArray();
                if (person.Length == 0)
                {
                    model.FirstName = model.FirstName.Trim();
                    model.LastName = model.LastName.Trim();
                    _context.Add(model);
                    _context.SaveChanges();
                    HttpContext.Session.SetString("Login", model.FirstName);
                    string message = model.FirstName + " " + model.LastName;
                    return RedirectToAction("Welcome", new RouteValueDictionary(new { controller = "Home", action = "Main", name = message }));
                }
                else
                {
                    if((model.FirstName == person[0].FirstName)&&(model.LastName == person[0].LastName))
                    {
                        HttpContext.Session.SetString("Login", model.FirstName);
                        string message = model.FirstName + " " + model.LastName;
                        return RedirectToAction("Welcome", new RouteValueDictionary(new { controller = "Home", action = "Main", name = message }));
                    }
                    else
                    {
                        ViewData["Warning"] = "This phone number already exists in DataBase, choose another";
                        return View(model);
                    }                   
                }                    
            }
            return View(model);
        }
        public IActionResult Welcome(string name)
        {
            ViewData["NAME"] = name;
            return View();
        }
        public IActionResult Error()
        {
            return View();
        }
    }
}
